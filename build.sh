#!/usr/bin/env bash

SCRIPTPATH=$(readlink -f "$0")
THISDIR=$(dirname "$SCRIPTPATH")

if [[ $(id -u) -ne 0 ]]; then
	echo "Please run this script with superuser privledges"
	exit
fi

if [[ ! -d build ]]; then
	echo "The build directory does not exist, please place a kernel in it"
	exit
fi

DEBIAN_MIRROR=${DEBIAN_MIRROR:-https://deb.debian.org/debian/}
DEBIAN_DIST=${DEBIAN_DIST:-stretch}

if [ ! -x "$(command -v pwgen)" ] && [ -z ${ROOT_PASSWORD+x} ]; then
	echo -n "pwgen is needed to generate a random root password, unless one is supplied with "
	echo "ROOT_PASSWORD"
	exit
fi

ROOT_PASSWORD=${ROOT_PASSWORD:-`pwgen 16 1`}

sleep 5

modprobe nbd
qemu-img create -f qcow2 debian.qcow2 2G
qemu-nbd -c /dev/nbd0 debian.qcow2
parted --script /dev/nbd0 \
    mklabel gpt \
    mkpart boot fat32 0% 128MiB \
    mkpart system ext4 128MiB 100% \
    set 1 boot on \
    set 1 esp on 

partx -a /dev/nbd0
mkfs.fat -F 32 -s 2 /dev/nbd0p1

mkfs.ext4 /dev/nbd0p2 

mkdir -p /mnt/debian/
mount /dev/nbd0p2 /mnt/debian/

mkdir -p /mnt/debian/boot/

mount /dev/nbd0p1 /mnt/debian/boot/


debootstrap --include=apt-transport-https,ca-certificates ${DEBIAN_DIST} /mnt/debian/ ${DEBIAN_MIRROR}
mount --bind /dev /mnt/debian/dev/
mount --bind /sys /mnt/debian/sys/
mount -t proc none /mnt/debian/proc/
cp /etc/resolv.conf /mnt/debian/etc/

chroot /mnt/debian /bin/bash --login -c "apt-get install -y --allow-unauthenticated grub-efi-arm64"

# Copy the kernel
cp build/vmlinuz* /mnt/debian/boot/
cp build/System.map* /mnt/debian/boot/
cp build/config* /mnt/debian/boot/
mkdir -p /mnt/debian/lib/modules/
cp -r build/mods/lib/modules/* /mnt/debian/lib/modules/
ls -la /mnt/debian/lib/modules/

chroot /mnt/debian /bin/bash --login -c 'grub-install --efi-directory=/boot --no-nvram --removable'

if [ ! -d /mnt/debian/etc/default/grub ]; then
	mkdir -p /mnt/debian/etc/default/grub/
fi

ROOT_DEVICE=`blkid /dev/nbd0p2 -o value -s PARTUUID`
cat <<EOF > /mnt/debian/etc/default/grub
GRUB_CMDLINE_LINUX_DEFAULT="console=ttyS0,115200 rootwait net.ifnames=0"
GRUB_DEVICE_UUID=${ROOT_DEVICE}
GRUB_TERMINAL="serial"
GRUB_SERIAL_COMMAND="serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1"
EOF

chroot /mnt/debian /bin/bash --login -c 'update-grub'

# As grub doesn't want to generate with a UUID (even when specified, force it)
sed -i "s/root=\/dev\/nbd0p2/root=PARTUUID=${ROOT_DEVICE}/g" /mnt/debian/boot/grub/grub.cfg
grep "PARTUUID=${ROOT_DEVICE}" /mnt/debian/boot/grub/grub.cfg
if [[ "$?" -ne 0 ]]; then
	echo "ERROR: PARTUUID is not in the generated grub.cfg, the script may need to be adjusted"
	echo "NOTE: /mnt/debian is still mounted for debugging purposes"
	exit
fi

echo "debian-arm64" > /mnt/debian/etc/hostname

cat <<EOF > /mnt/debian/etc/network/interfaces.d/eth0
auto eth0
iface eth0 inet dhcp
EOF

chroot /mnt/debian/ /bin/sh -c "echo root:$ROOT_PASSWORD | chpasswd; echo \"*** root password is $ROOT_PASSWORD ***\""
chroot /mnt/debian /bin/bash --login -c "depmod -a `ls /lib/modules/`"

BOOT_DEVICE=`blkid /dev/nbd0p1 -o value -s PARTUUID`
cat <<EOF > /mnt/debian/etc/fstab
PARTUUID=${ROOT_DEVICE}		/		ext4	defaults	1	1
PARTUUID=${BOOT_DEVICE}		/boot	vfat	defaults	1	1
EOF

# gpg-agent seems to hang around, kill it
chroot /mnt/debian /bin/sh -c "killall gpg-agent"

 # Teardown
umount -R /mnt/debian
qemu-nbd -d /dev/nbd0
echo $?
if [[ "$?" -eq 0 ]]; then
       qemu-nbd -d /dev/nbd0
fi
